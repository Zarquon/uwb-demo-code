import serial


def convertToInt(mantissa_str):
    power_count = -1
    mantissa_int = 0
    for i in mantissa_str:
        mantissa_int += (int(i) * pow(2, power_count))
        power_count -= 1
    return (mantissa_int + 1)

# Converts binary string to float
def bin_to_float(input):
    result = int(input[1:9], 2)
    exponent_unbias = result - 127
    sign_bit = int(input[0])
    mantissa_int = convertToInt(input[9:])
    real_no = pow(-1, sign_bit) * mantissa_int * pow(2, exponent_unbias)
    return(real_no)

# Converts input from big endian to little endian.
def four_byte_convert(input):
    newString = input[6:8] + input[4:6] + input[2:4] + input[0:2]
    return(bin_to_float(format(int(newString, 16), "032b")))


#'/dev/tty.wchusbserial14340'
portName = input("Port address: ")
serialPort = serial.Serial(port=portName, baudrate=115200,
                           bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE)

while True:
    hexStringStart = serialPort.read(1).hex()
    if hexStringStart == 'ff':
        output = hexStringStart + serialPort.read(127).hex()
        range = four_byte_convert(output[8:16])
        print(range)
